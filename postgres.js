'use strict';

const Sequelize = require('sequelize');

function postgresDatabase(config) {
  return new PostgresDatabase(config);
}

postgresDatabase.$inject = ['config'];

class PostgresDatabase {

  constructor(config) {
    this._config = config;
  }

  async connect() {
    let config = this._config.postgres;

    if (!config) {
      return;
    }

    this._sequelize = new Sequelize(config.name, config.username, config.password, {...config.options});
    try {
      await this._sequelize.authenticate();
      console.log('SQL database successfully connected');
    } catch (err) {
      console.log('Unable to connect to database');
    }
  }

  disconnect() {
    if (this._sequelize) {
      return this._sequelize.close();
    }
  }

  createModel(name, fields, options = {}) {
    if (this._sequelize) {
      return this._sequelize.define(name, fields, options);
    }
  }

  get Sequelize() {
    if (this._sequelize) {
      return this._sequelize.Sequelize;
    }
    return null;
  }

  get sequelize() {
    return this._sequelize;
  }

}

module.exports = postgresDatabase;
