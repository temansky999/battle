const path = require('path');
const pm2 = require('pm2');

let application = path.join(__dirname, '..', 'start.js');
let file = path.join(__dirname, '..', 'log', 'access.log');

pm2.connect(error => {
  if (error) {
    throw error;
  }

  pm2.start(application, {
    name: 'BATTLE-APP',
    output: file,
    error: file,
    mergeLogs: true,
    instances: 0,
    exec_mode: "cluster"
  }, err => {
    if (err) {
      throw err;
    }

    pm2.disconnect(() => {
      process.exit(0);
    });
  });
});
