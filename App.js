const intravenous = require('./intravenous');
const http = require('http');
const express = require('express');
const bodyParser = require('body-parser');

const config = require('./config');

const postgresDatabase = require('./postgres');
const versionRouter = require('./app/version/version.router');
const calculateService = require('./app/calculate/calculate.service');
const calculateRouter = require('./app/calculate/calculate.router');
const createService = require('./app/create/create.service');
const createRouter = require('./app/create/create.router');
const userModel = require('./app/user/user.model');
const userService = require('./app/user/user.service');
const userRouter = require('./app/user/user.router');

class App {
  constructor() {
    this._routers = [];
    this._container = intravenous.create();
  }

  registerService(id, service, scope = 'singleton') {
    this._container.register(id, service, scope || 'singleton');
  }

  async start(port) {
    this.registerDependencies();
    this.registerRoutes();

    await this.connectToDatabase();

    await this._container.get('userModel').User.sync();
    this.startServer(port);
  }

  registerDependencies() {
    this.registerService('config', config);
    this.registerService('postgresDatabase', postgresDatabase);
    this.registerService('versionRouter', versionRouter);
    this.registerService('calculateService', calculateService);
    this.registerService('calculateRouter', calculateRouter);
    this.registerService('createService', createService);
    this.registerService('createRouter', createRouter);
    this.registerService('userModel', userModel);
    this.registerService('userService', userService);
    this.registerService('userRouter', userRouter);
  }

  registerRoutes() {
    this.addRouter('/', container => container.get('versionRouter').api());
    this.addRouter('/', container => container.get('calculateRouter').api());
    this.addRouter('/', container => container.get('createRouter').api());
    this.addRouter('/', container => container.get('userRouter').api());
  }

  async connectToDatabase() {
    await this._container.get('postgresDatabase').connect();
  }

  errorHandling() {
    this._server.use((req, res, next) => {
      let err = new Error(`Can't find path ${req.originalUrl}`);
      err.status = 404;
      next(err);
    });


    this._server.use((err, req, res, next) => {
      console.log(err);

      if (err.code) {
        err.status = err.code;
      } else {
        err.status = 500;
      }

      res.status(err.status);
      let result = {
        error: {
          message: err.message
        }
      };

      if (err.status === 500 && req.app.get('env') === 'production') {
        result.error.message = 'INTERNAL_SERVER_ERROR';
      }

      res.json(result);
    });
  }

  startServer(port) {
    this._server = express();
    this._httpServer = http.createServer(this._server);

    this._server.use(bodyParser({limit: '50mb'}));
    this._server.use(bodyParser.urlencoded({extended: false, limit: '50mb'}));
    this._server.use(bodyParser.json({limit: '50mb'}));
    this._server.use(bodyParser.text({limit: '50mb'}));

    this._routers.forEach(definition => {
      this._server.use(definition.url, definition.provider(this._container));
    });

    this._connection = this._httpServer.listen(port, () => {
      console.log(`Server connected on ${port}`)
    });
  }

  get server() {
    return this._server;
  }

  addRouter(url, provider) {
    this._routers.push({
      url: url,
      provider: provider
    });
  }

  async stop() {
    this._server = null;
    this._routers = [];
    this._container = null;

    if (this._connection) {
      return new Promise((resolve, reject) => {
        this._connection.close((err) => {
          err ? reject(err) : resolve();
        });
      });
    }
  }
}

module.exports = App;