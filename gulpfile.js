'use strict';

const gulp = require('gulp'),
  mocha = require('gulp-mocha'),
  util = require('gulp-util');

gulp.paths = {
    src: './app'
  };

const paths = gulp.paths;

gulp.task('unit', () => {
  process.env.NODE_ENV = 'test';
  console.log(util.env.tags);
  let src = util.env.tags ?
    [`${paths.src}/**/${util.env.tags}`] :
    [paths.src + '/**/*.spec.js', '!**/public**', '!gulpfile.js', '!app/start.es6', '!app/app.js'];

  return gulp.src(src)
    .pipe(mocha({timeout: 15000}))
    .once('end', function (exitCode) {
      setTimeout(process.exit.bind(null, exitCode), 5000);
    });
});

