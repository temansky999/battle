'use strict';

const express = require('express');

function createRouter(createService) {
  return new CreateRouter(createService);
}
createRouter.$inject = ['createService'];

class CreateRouter {
  constructor(createService) {
    this._createService = createService;
  }

  api() {
    let router = express.Router();

    router.route('/create')
      .post(this._createService.generateArray);

    return router;
  }

}

module.exports = createRouter;
