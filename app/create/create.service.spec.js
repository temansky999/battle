'use strict';

require('should');
const request = require('supertest-promised');
const BattleAPI = require('../../App');

let server,
  app = new BattleAPI();

describe('Create tests', () => {

  before(async () => {
    await app.start(8080);
    server = app.server;
  });

  after(async () => {
    await app.stop();
  });

  describe('POST /create', () => {

    it('should return range items count', async () => {
      let body = await request(server)
        .post('/create')
        .set('Accept', 'application/json')
        .send({
          num: 40,
          min: 0,
          max: 15
        })
        .expect(200)
        .end()
        .get('body');

      (typeof body.count).should.be.eql('number');
    });
  });

});
