'use strict';

const createError = require('http-errors');

function createService() {
  return new CreateService();
}

class CreateService {

  get generateArray() {
    return this._generateArray.bind(this);
  }

  _generateArray(req, res, next) {
    let body = req.body;

    if (isNaN(parseInt(body.num))) {
      return next(createError(422,'Incorrect num param'));
    }

    if (typeof body.min !== 'number') {
      return next(createError(422, 'Incorrect min param'));
    }

    if (typeof body.max !== 'number') {
      return next(createError(422, 'Incorrect max param'));
    }

    if (body.min >= body.max) {
      return next(createError(422, 'Min param must be less than max param'));
    }

    let count = Array
      .from({length: body.num}, () => Math.random())
      .filter(item => item >= body.min && item <= body.max)
      .length;

    res.json({count});
  }
}


module.exports = createService;