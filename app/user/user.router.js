'use strict';

const express = require('express');

function userRouter(userService) {
  return new UserRouter(userService);
}
userRouter.$inject = ['userService'];

class UserRouter {
  constructor(userService) {
    this._userService = userService;
  }

  api() {
    let router = express.Router();

    router.route('/user')
      .get(this._userService.list)
      .post(this._userService.create)
      .delete(this._userService.delete);

    router.route('/user/:id')
      .get(this._userService.single);

    return router;
  }

}

module.exports = userRouter;
