'use strict';

const createError = require('http-errors');

function userService(userModel) {
  return new UserService(userModel);
}

userService.$inject = ['userModel'];

class UserService {
  constructor(userModel) {
    this._userModel = userModel;
  }

  get create() {
    return this._create.bind(this);
  }

  get list() {
    return this._list.bind(this);
  }

  get update() {
    return this._update.bind(this);
  }

  get delete() {
    return this._delete.bind(this);
  }

  get single() {
    return this._single.bind(this);
  }

  async _create(req, res, next) {
    let body = req.body;

    if (typeof body.fname !== 'string') {
      throw createError(422, 'Incorrect fname');
    }

    if (typeof body.lname !== 'string') {
      throw createError(422, 'Incorrect lname');
    }

    if (typeof body.age !== 'number') {
      throw createError(422, 'Incorrect age');
    }

    if (!['M', 'F'].includes(body.sex)) {
      throw createError(422, 'Incorrect sex');
    }

    let newUser = await new this._userModel.User(body).save();

    res.status(201);
    res.send(newUser);
    res.end();
  }

  async _list(req, res, next) {
    try {
      let list = await this._userModel.User.findAll();
      res.json(list);
    } catch (err) {
      return next(err);
    }
  }

  async _update(req, res, next) {

  }

  async _delete(req, res, next) {
    try {
      let id = req.params.id;
      let user = await this._userModel.User.findById(id);
      await user.destroy();

      res.status(204);
      res.send();
      res.end();
    } catch (err) {
      return next(err);
    }
  }

  async _single(req, res, next) {
    try {
      let id = req.params.id;
      let user = await this._userModel.User.findById(id);

      res.json(user);
    } catch (err) {
      return next(err);
    }
  }

}


module.exports = userService;