'use strict';

require('should');
const request = require('supertest-promised');
const BattleAPI = require('../../App');

let server,
  app = new BattleAPI();

describe('User tests', () => {

  before(async () => {
    await app.start(8080);
    server = app.server;
  });

  after(async () => {
    await app.stop();
  });

  describe('POST /user', () => {

    it('should create user', async () => {
      let body = await request(server)
        .post('/user')
        .set('Accept', 'application/json')
        .send({
          fname: 'Artem',
          lname: 'Shorin',
          age: 23,
          sex: 'M'
        })
        .expect(201)
        .end()
        .get('body');

      body.id.should.not.be.null();
      body.fname.should.be.eql('Artem');
      body.lname.should.be.eql('Shorin');
      body.age.should.be.eql(23);
      body.sex.should.be.eql('M');
    });
  });

  describe('GET /user', () => {

    it('should create user', async () => {
      let body = await request(server)
        .get('/user')
        .set('Accept', 'application/json')
        .send()
        .expect(200)
        .end()
        .get('body');

      console.log(body);
    });
  });

});
