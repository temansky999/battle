'use strict';

function userModel(postgresDatabase) {
  return new UserModel(postgresDatabase);
}

userModel.$inject = ['postgresDatabase'];

class UserModel {

  constructor(postgresDatabase) {
    const Sequelize = postgresDatabase.Sequelize;

    let fields = {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      fname: {
        type: Sequelize.TEXT,
        allowNull: false
      },
      lname: {
        type: Sequelize.TEXT,
        allowNull: false
      },
      age: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      sex: {
        type: Sequelize.ENUM('M', 'F'),
        allowNull: false
      }
    };

    let options = {
      timestamps: false,
      underscored: true,
      tableName: 'users'
    };

    this._User = postgresDatabase.createModel('users', fields, options);
  }

  get User() {
    return this._User;
  }

}

module.exports = userModel;
