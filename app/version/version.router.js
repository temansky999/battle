'use strict';

const express = require('express');

function versionRouter() {
  return new VersionRouter();
}

class VersionRouter {
  api() {
    let router = express.Router();

    router.route('/version')
      .get((req, res) => {
        res.json({version: '1.0', status: 'active'});
      });

    return router;
  }

}

module.exports = versionRouter;
