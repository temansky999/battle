'use strict';

require('should');
const request = require('supertest-promised');
const BattleAPI = require('../../App');

let server,
  app = new BattleAPI();

describe('Calculate tests', () => {

  before(async () => {
    await app.start(8080);
    server = app.server;
  });

  after(async () => {
    await app.stop();
  });

  describe('POST /calculate', () => {

    it('should calculate 2 numbers', async () => {
      let body = await request(server)
        .post('/calculate')
        .set('Accept', 'application/json')
        .send({
          a: 1,
          b: 4
        })
        .expect(200)
        .end()
        .get('body');

      body.summ.should.be.eql(5);
    });
  });

});
