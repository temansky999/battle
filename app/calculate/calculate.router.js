'use strict';

const express = require('express');

function calculateRouter(calculateService) {
  return new CalculateRouter(calculateService);
}
calculateRouter.$inject = ['calculateService'];

class CalculateRouter {
  constructor(calculateService) {
    this._calculateService = calculateService;
  }

  api() {
    let router = express.Router();

    router.route('/calculate')
      .post(this._calculateService.calculateSum);

    return router;
  }

}

module.exports = calculateRouter;
