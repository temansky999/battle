'use strict';

const createError = require('http-errors');

function calculateService() {
 return new CalculateService();
}

class CalculateService {

  get calculateSum() {
    return this._calculateSum.bind(this);
  }

  _calculateSum(req, res, next) {
    let body = req.body;

    if (!body.a || typeof body.a !== 'number') {
      return next(createError(422, 'Validation error'));
    }

    if (!body.b || typeof body.b !== 'number') {
      return next(createError(422, 'Validation error'));
    }

    res.json({
      summ: body.a + body.b
    });
  }
}


module.exports = calculateService;